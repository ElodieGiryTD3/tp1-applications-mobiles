package fr.uavignon.ceri.tp1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import fr.uavignon.ceri.tp1.data.Country;
import fr.uavignon.tp1.SecondFragmentArgs;

public class DetailFragment extends Fragment {

    public static final String TAG = "SecondFragment";
    TextView textView;
    ImageView imageView;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //// Implementation with bundle
        // textView.setText("Info in chapter "+(getArguments().getInt("numChapter")+1));

        textView = view.findViewById(R.id.valeurPays);
        SecondFragmentArgs args = SecondFragmentArgs.fromBundle(getArguments());
        textView.setText(Country.countries[args.getKeyChapterId()].getName());

        imageView = view.findViewById(R.id.imagePays);

/*
        String uri = Country.countries[i].getImgUri();
        int tmpi = getContext().getResources().getIdentifier(uri, "drawable","fr.uavignon.recyclerviewdemo")
        Context c = viewHolder.itemImage.getContext();
        viewHolder.itemImage.setImageDrawable(c.getResources().getDrawable(
                c.getResources(). getIdentifier (uri , null , c.getPackageName())));
*/

        ////////////////////////////////////////////////////////
        //String imagePays = "R.drawable"+Country.countries[args.getKeyChapterId()].getImgUri()+"_320px";
        //String imagePays = "ic_"+Country.countries[args.getKeyChapterId()].getImgUri()+"_320px";
        //imageView.setImageResource(Integer.parseInt(imagePays));

        /*
        String imagePays = Country.countries[args.getKeyChapterId()].getImgUri()+"_320px";
        Field fld = null;
        try {
            fld = R.drawable.class.getField(imagePays);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        int tmpInt = 0;
        try {
            tmpInt = fld.getInt(null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        imageView.setImageResource(tmpInt);
*/

        //imageView.setImageURI(Uri.parse("@drawable/"+ Country.countries[args.getKeyChapterId()].getImgUri()+"_320px"));
        //android:src="@drawable/ic_flag_of_france_320px"

        textView.setText(Country.countries[args.getKeyChapterId()].getName());

        textView = view.findViewById(R.id.valeurCapitale);
        textView.setText(Country.countries[args.getKeyChapterId()].getCapital());

        textView = view.findViewById(R.id.valeurLangue);
        textView.setText(Country.countries[args.getKeyChapterId()].getLanguage());

        textView = view.findViewById(R.id.valeurMonnaie);
        textView.setText(Country.countries[args.getKeyChapterId()].getCurrency());

        textView = view.findViewById(R.id.valeurPopulation);
        textView.setText(String.valueOf(Country.countries[args.getKeyChapterId()].getPopulation()));

        textView = view.findViewById(R.id.valeurSuperficie);
        textView.setText(String.valueOf(Country.countries[args.getKeyChapterId()].getArea()+" km²"));



        Bundle bundle = new Bundle();
        /*
        bundle.putString("lastName", editTextLastName.getText().toString());
        bundle.putString("firstName", editTextFirstName.getText().toString());

         */


        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });


    }
}